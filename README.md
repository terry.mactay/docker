# Docker

## What docker does?
Where virtualization and virtual machines are virtualizing hardware, docker virtualizes the operating system. 
We see our visualization works; we have the hardware, the hypervisor, we have our guest operating systems.
With docker, we’ll always have the hardware, but instead of a hypervisor, we install one operating system on top of this hardware. 

Here, we are installing Ubuntu. then we have docker, docker installs like any other application you might install on Linux, it’ll be a process running or a daemon running and then using docker, we will virtualize portions of our operating system, in this case, Ubuntu to create containers.
A container is fast lightweight microcomputers, they’ll have their operating system, their CPU processes, memory, network and their quarantine.

It’s time to find out why Docker is containers is fast and lightweight.

First, let test by running CentOS
```
docker pull centos
```
Then we ask docker to run our image.
```
docker run -d -t --name myfirstimage centos
```
To see our running container, we'll use this command:
```
docker ps
```
We can connect to our centos and play around with it with this command:
```
docker exec -it myfirstimage bash
```
Let get Jenkins from DockerHub, we can use the command `docker pull jenkins` but let try something like using the `tag` this enables us to pull different version of `Jenkins` the command is:
```
docker pull jenkins:latest
```
Now let run the `Jenkins image` and map it with a port
```
docker run -t -d 80:80 --name myjenkins jenkins:latest
```
If we want to check how much CPU, memory, the network is using:
```
docker stats
```
We are done!!


